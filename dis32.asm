
hw2prog-32:     file format elf32-i386
hw2prog-32
architecture: i386, flags 0x00000112:
EXEC_P, HAS_SYMS, D_PAGED
start address 0x080484b0

Program Header:
    PHDR off    0x00000034 vaddr 0x08048034 paddr 0x08048034 align 2**2
         filesz 0x00000100 memsz 0x00000100 flags r-x
  INTERP off    0x00000134 vaddr 0x08048134 paddr 0x08048134 align 2**0
         filesz 0x00000013 memsz 0x00000013 flags r--
    LOAD off    0x00000000 vaddr 0x08048000 paddr 0x08048000 align 2**12
         filesz 0x000009bc memsz 0x000009bc flags r-x
    LOAD off    0x000009bc vaddr 0x080499bc paddr 0x080499bc align 2**12
         filesz 0x00000120 memsz 0x0000015c flags rw-
 DYNAMIC off    0x000009d0 vaddr 0x080499d0 paddr 0x080499d0 align 2**2
         filesz 0x000000c8 memsz 0x000000c8 flags rw-
    NOTE off    0x00000148 vaddr 0x08048148 paddr 0x08048148 align 2**2
         filesz 0x00000044 memsz 0x00000044 flags r--
EH_FRAME off    0x00000948 vaddr 0x08048948 paddr 0x08048948 align 2**2
         filesz 0x0000001c memsz 0x0000001c flags r--
   STACK off    0x00000000 vaddr 0x00000000 paddr 0x00000000 align 2**2
         filesz 0x00000000 memsz 0x00000000 flags rw-

Dynamic Section:
  NEEDED               libc.so.6
  INIT                 0x080483c8
  FINI                 0x080488dc
  GNU_HASH             0x0804818c
  STRTAB               0x08048298
  SYMTAB               0x080481b8
  STRSZ                0x00000089
  SYMENT               0x00000010
  DEBUG                0x00000000
  PLTGOT               0x08049a9c
  PLTRELSZ             0x00000050
  PLTREL               0x00000011
  JMPREL               0x08048378
  REL                  0x08048360
  RELSZ                0x00000018
  RELENT               0x00000008
  VERNEED              0x08048340
  VERNEEDNUM           0x00000001
  VERSYM               0x08048322

Version References:
  required from libc.so.6:
    0x0d696910 0x00 02 GLIBC_2.0

Sections:
Idx Name          Size      VMA       LMA       File off  Algn
  0 .interp       00000013  08048134  08048134  00000134  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  1 .note.ABI-tag 00000020  08048148  08048148  00000148  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  2 .note.gnu.build-id 00000024  08048168  08048168  00000168  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  3 .gnu.hash     0000002c  0804818c  0804818c  0000018c  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  4 .dynsym       000000e0  080481b8  080481b8  000001b8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  5 .dynstr       00000089  08048298  08048298  00000298  2**0
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  6 .gnu.version  0000001c  08048322  08048322  00000322  2**1
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  7 .gnu.version_r 00000020  08048340  08048340  00000340  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  8 .rel.dyn      00000018  08048360  08048360  00000360  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  9 .rel.plt      00000050  08048378  08048378  00000378  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 10 .init         00000030  080483c8  080483c8  000003c8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 11 .plt          000000b0  080483f8  080483f8  000003f8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 12 .text         0000042c  080484b0  080484b0  000004b0  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 13 .fini         0000001c  080488dc  080488dc  000008dc  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 14 .rodata       0000004e  080488f8  080488f8  000008f8  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 15 .eh_frame_hdr 0000001c  08048948  08048948  00000948  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 16 .eh_frame     00000058  08048964  08048964  00000964  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 17 .ctors        00000008  080499bc  080499bc  000009bc  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 18 .dtors        00000008  080499c4  080499c4  000009c4  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 19 .jcr          00000004  080499cc  080499cc  000009cc  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 20 .dynamic      000000c8  080499d0  080499d0  000009d0  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 21 .got          00000004  08049a98  08049a98  00000a98  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 22 .got.plt      00000034  08049a9c  08049a9c  00000a9c  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 23 .data         0000000c  08049ad0  08049ad0  00000ad0  2**2
                  CONTENTS, ALLOC, LOAD, DATA
 24 .bss          00000038  08049ae0  08049ae0  00000adc  2**5
                  ALLOC
 25 .comment      0000010e  00000000  00000000  00000adc  2**0
                  CONTENTS, READONLY
SYMBOL TABLE:
no symbols



Disassembly of section .interp:

08048134 <.interp>:
 8048134:	2f                   	das    
 8048135:	6c                   	ins    BYTE PTR es:[edi],dx
 8048136:	69 62 2f 6c 64 2d 6c 	imul   esp,DWORD PTR [edx+0x2f],0x6c2d646c
 804813d:	69 6e 75 78 2e 73 6f 	imul   ebp,DWORD PTR [esi+0x75],0x6f732e78
 8048144:	2e 32 00             	xor    al,BYTE PTR cs:[eax]

Disassembly of section .note.ABI-tag:

08048148 <.note.ABI-tag>:
 8048148:	04 00                	add    al,0x0
 804814a:	00 00                	add    BYTE PTR [eax],al
 804814c:	10 00                	adc    BYTE PTR [eax],al
 804814e:	00 00                	add    BYTE PTR [eax],al
 8048150:	01 00                	add    DWORD PTR [eax],eax
 8048152:	00 00                	add    BYTE PTR [eax],al
 8048154:	47                   	inc    edi
 8048155:	4e                   	dec    esi
 8048156:	55                   	push   ebp
 8048157:	00 00                	add    BYTE PTR [eax],al
 8048159:	00 00                	add    BYTE PTR [eax],al
 804815b:	00 02                	add    BYTE PTR [edx],al
 804815d:	00 00                	add    BYTE PTR [eax],al
 804815f:	00 06                	add    BYTE PTR [esi],al
 8048161:	00 00                	add    BYTE PTR [eax],al
 8048163:	00 09                	add    BYTE PTR [ecx],cl
 8048165:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .note.gnu.build-id:

08048168 <.note.gnu.build-id>:
 8048168:	04 00                	add    al,0x0
 804816a:	00 00                	add    BYTE PTR [eax],al
 804816c:	14 00                	adc    al,0x0
 804816e:	00 00                	add    BYTE PTR [eax],al
 8048170:	03 00                	add    eax,DWORD PTR [eax]
 8048172:	00 00                	add    BYTE PTR [eax],al
 8048174:	47                   	inc    edi
 8048175:	4e                   	dec    esi
 8048176:	55                   	push   ebp
 8048177:	00 06                	add    BYTE PTR [esi],al
 8048179:	25 08 0f ca d1       	and    eax,0xd1ca0f08
 804817e:	5d                   	pop    ebp
 804817f:	65                   	gs
 8048180:	5a                   	pop    edx
 8048181:	59                   	pop    ecx
 8048182:	25 64 e0 bc 06       	and    eax,0x6bce064
 8048187:	7f d5                	jg     804815e <mmap@plt-0x2aa>
 8048189:	4a                   	dec    edx
 804818a:	a5                   	movs   DWORD PTR es:[edi],DWORD PTR ds:[esi]
 804818b:	58                   	pop    eax

Disassembly of section .gnu.hash:

0804818c <.gnu.hash>:
 804818c:	03 00                	add    eax,DWORD PTR [eax]
 804818e:	00 00                	add    BYTE PTR [eax],al
 8048190:	0b 00                	or     eax,DWORD PTR [eax]
 8048192:	00 00                	add    BYTE PTR [eax],al
 8048194:	01 00                	add    DWORD PTR [eax],eax
 8048196:	00 00                	add    BYTE PTR [eax],al
 8048198:	05 00 00 00 00       	add    eax,0x0
 804819d:	23 02                	and    eax,DWORD PTR [edx]
 804819f:	22 0b                	and    cl,BYTE PTR [ebx]
 80481a1:	00 00                	add    BYTE PTR [eax],al
 80481a3:	00 0c 00             	add    BYTE PTR [eax+eax*1],cl
 80481a6:	00 00                	add    BYTE PTR [eax],al
 80481a8:	00 00                	add    BYTE PTR [eax],al
 80481aa:	00 00                	add    BYTE PTR [eax],al
 80481ac:	29 1d 8c 1c ac 4b    	sub    DWORD PTR ds:0x4bac1c8c,ebx
 80481b2:	e3 c0                	jecxz  8048174 <mmap@plt-0x294>
 80481b4:	39 f2                	cmp    edx,esi
 80481b6:	8b                   	.byte 0x8b
 80481b7:	1c                   	.byte 0x1c

Disassembly of section .dynsym:

080481b8 <.dynsym>:
	...
 80481c8:	30 00                	xor    BYTE PTR [eax],al
	...
 80481d2:	00 00                	add    BYTE PTR [eax],al
 80481d4:	12 00                	adc    al,BYTE PTR [eax]
 80481d6:	00 00                	add    BYTE PTR [eax],al
 80481d8:	29 00                	sub    DWORD PTR [eax],eax
	...
 80481e2:	00 00                	add    BYTE PTR [eax],al
 80481e4:	12 00                	adc    al,BYTE PTR [eax]
 80481e6:	00 00                	add    BYTE PTR [eax],al
 80481e8:	01 00                	add    DWORD PTR [eax],eax
	...
 80481f2:	00 00                	add    BYTE PTR [eax],al
 80481f4:	20 00                	and    BYTE PTR [eax],al
 80481f6:	00 00                	add    BYTE PTR [eax],al
 80481f8:	35 00 00 00 00       	xor    eax,0x0
 80481fd:	00 00                	add    BYTE PTR [eax],al
 80481ff:	00 00                	add    BYTE PTR [eax],al
 8048201:	00 00                	add    BYTE PTR [eax],al
 8048203:	00 12                	add    BYTE PTR [edx],dl
 8048205:	00 00                	add    BYTE PTR [eax],al
 8048207:	00 3c 00             	add    BYTE PTR [eax+eax*1],bh
	...
 8048212:	00 00                	add    BYTE PTR [eax],al
 8048214:	12 00                	adc    al,BYTE PTR [eax]
 8048216:	00 00                	add    BYTE PTR [eax],al
 8048218:	6d                   	ins    DWORD PTR es:[edi],dx
	...
 8048221:	00 00                	add    BYTE PTR [eax],al
 8048223:	00 12                	add    BYTE PTR [edx],dl
 8048225:	00 00                	add    BYTE PTR [eax],al
 8048227:	00 4a 00             	add    BYTE PTR [edx+0x0],cl
	...
 8048232:	00 00                	add    BYTE PTR [eax],al
 8048234:	12 00                	adc    al,BYTE PTR [eax]
 8048236:	00 00                	add    BYTE PTR [eax],al
 8048238:	58                   	pop    eax
	...
 8048241:	00 00                	add    BYTE PTR [eax],al
 8048243:	00 12                	add    BYTE PTR [edx],dl
 8048245:	00 00                	add    BYTE PTR [eax],al
 8048247:	00 5f 00             	add    BYTE PTR [edi+0x0],bl
	...
 8048252:	00 00                	add    BYTE PTR [eax],al
 8048254:	12 00                	adc    al,BYTE PTR [eax]
 8048256:	00 00                	add    BYTE PTR [eax],al
 8048258:	67 00 00             	add    BYTE PTR [bx+si],al
	...
 8048263:	00 12                	add    BYTE PTR [edx],dl
 8048265:	00 00                	add    BYTE PTR [eax],al
 8048267:	00 43 00             	add    BYTE PTR [ebx+0x0],al
 804826a:	00 00                	add    BYTE PTR [eax],al
 804826c:	00 9b 04 08 04 00    	add    BYTE PTR [ebx+0x40804],bl
 8048272:	00 00                	add    BYTE PTR [eax],al
 8048274:	11 00                	adc    DWORD PTR [eax],eax
 8048276:	19 00                	sbb    DWORD PTR [eax],eax
 8048278:	1a 00                	sbb    al,BYTE PTR [eax]
 804827a:	00 00                	add    BYTE PTR [eax],al
 804827c:	fc                   	cld    
 804827d:	88 04 08             	mov    BYTE PTR [eax+ecx*1],al
 8048280:	04 00                	add    al,0x0
 8048282:	00 00                	add    BYTE PTR [eax],al
 8048284:	11 00                	adc    DWORD PTR [eax],eax
 8048286:	0f 00 51 00          	lldt   WORD PTR [ecx+0x0]
 804828a:	00 00                	add    BYTE PTR [eax],al
 804828c:	e0 9a                	loopne 8048228 <mmap@plt-0x1e0>
 804828e:	04 08                	add    al,0x8
 8048290:	04 00                	add    al,0x0
 8048292:	00 00                	add    BYTE PTR [eax],al
 8048294:	11 00                	adc    DWORD PTR [eax],eax
 8048296:	19 00                	sbb    DWORD PTR [eax],eax

Disassembly of section .dynstr:

08048298 <.dynstr>:
 8048298:	00 5f 5f             	add    BYTE PTR [edi+0x5f],bl
 804829b:	67 6d                	ins    DWORD PTR es:[di],dx
 804829d:	6f                   	outs   dx,DWORD PTR ds:[esi]
 804829e:	6e                   	outs   dx,BYTE PTR ds:[esi]
 804829f:	5f                   	pop    edi
 80482a0:	73 74                	jae    8048316 <mmap@plt-0xf2>
 80482a2:	61                   	popa   
 80482a3:	72 74                	jb     8048319 <mmap@plt-0xef>
 80482a5:	5f                   	pop    edi
 80482a6:	5f                   	pop    edi
 80482a7:	00 6c 69 62          	add    BYTE PTR [ecx+ebp*2+0x62],ch
 80482ab:	63 2e                	arpl   WORD PTR [esi],bp
 80482ad:	73 6f                	jae    804831e <mmap@plt-0xea>
 80482af:	2e 36 00 5f 49       	cs add BYTE PTR cs:ss:[edi+0x49],bl
 80482b4:	4f                   	dec    edi
 80482b5:	5f                   	pop    edi
 80482b6:	73 74                	jae    804832c <mmap@plt-0xdc>
 80482b8:	64 69 6e 5f 75 73 65 	imul   ebp,DWORD PTR fs:[esi+0x5f],0x64657375
 80482bf:	64 
 80482c0:	00 67 65             	add    BYTE PTR [edi+0x65],ah
 80482c3:	74 70                	je     8048335 <mmap@plt-0xd3>
 80482c5:	69 64 00 6d 6d 61 70 	imul   esp,DWORD PTR [eax+eax*1+0x6d],0x70616d 
 80482cc:	00 
 80482cd:	63 61 6c             	arpl   WORD PTR [ecx+0x6c],sp
 80482d0:	6c                   	ins    BYTE PTR es:[edi],dx
 80482d1:	6f                   	outs   dx,DWORD PTR ds:[esi]
 80482d2:	63 00                	arpl   WORD PTR [eax],ax
 80482d4:	6d                   	ins    DWORD PTR es:[edi],dx
 80482d5:	65                   	gs
 80482d6:	6d                   	ins    DWORD PTR es:[edi],dx
 80482d7:	73 65                	jae    804833e <mmap@plt-0xca>
 80482d9:	74 00                	je     80482db <mmap@plt-0x12d>
 80482db:	73 74                	jae    8048351 <mmap@plt-0xb7>
 80482dd:	64 6f                	outs   dx,DWORD PTR fs:[esi]
 80482df:	75 74                	jne    8048355 <mmap@plt-0xb3>
 80482e1:	00 6d 65             	add    BYTE PTR [ebp+0x65],ch
 80482e4:	6d                   	ins    DWORD PTR es:[edi],dx
 80482e5:	63 70 79             	arpl   WORD PTR [eax+0x79],si
 80482e8:	00 73 74             	add    BYTE PTR [ebx+0x74],dh
 80482eb:	64                   	fs
 80482ec:	65                   	gs
 80482ed:	72 72                	jb     8048361 <mmap@plt-0xa7>
 80482ef:	00 66 77             	add    BYTE PTR [esi+0x77],ah
 80482f2:	72 69                	jb     804835d <mmap@plt-0xab>
 80482f4:	74 65                	je     804835b <mmap@plt-0xad>
 80482f6:	00 66 70             	add    BYTE PTR [esi+0x70],ah
 80482f9:	72 69                	jb     8048364 <mmap@plt-0xa4>
 80482fb:	6e                   	outs   dx,BYTE PTR ds:[esi]
 80482fc:	74 66                	je     8048364 <mmap@plt-0xa4>
 80482fe:	00 73 6c             	add    BYTE PTR [ebx+0x6c],dh
 8048301:	65                   	gs
 8048302:	65                   	gs
 8048303:	70 00                	jo     8048305 <mmap@plt-0x103>
 8048305:	5f                   	pop    edi
 8048306:	5f                   	pop    edi
 8048307:	6c                   	ins    BYTE PTR es:[edi],dx
 8048308:	69 62 63 5f 73 74 61 	imul   esp,DWORD PTR [edx+0x63],0x6174735f
 804830f:	72 74                	jb     8048385 <mmap@plt-0x83>
 8048311:	5f                   	pop    edi
 8048312:	6d                   	ins    DWORD PTR es:[edi],dx
 8048313:	61                   	popa   
 8048314:	69 6e 00 47 4c 49 42 	imul   ebp,DWORD PTR [esi+0x0],0x42494c47
 804831b:	43                   	inc    ebx
 804831c:	5f                   	pop    edi
 804831d:	32 2e                	xor    ch,BYTE PTR [esi]
 804831f:	30 00                	xor    BYTE PTR [eax],al

Disassembly of section .gnu.version:

08048322 <.gnu.version>:
 8048322:	00 00                	add    BYTE PTR [eax],al
 8048324:	02 00                	add    al,BYTE PTR [eax]
 8048326:	02 00                	add    al,BYTE PTR [eax]
 8048328:	00 00                	add    BYTE PTR [eax],al
 804832a:	02 00                	add    al,BYTE PTR [eax]
 804832c:	02 00                	add    al,BYTE PTR [eax]
 804832e:	02 00                	add    al,BYTE PTR [eax]
 8048330:	02 00                	add    al,BYTE PTR [eax]
 8048332:	02 00                	add    al,BYTE PTR [eax]
 8048334:	02 00                	add    al,BYTE PTR [eax]
 8048336:	02 00                	add    al,BYTE PTR [eax]
 8048338:	02 00                	add    al,BYTE PTR [eax]
 804833a:	01 00                	add    DWORD PTR [eax],eax
 804833c:	02 00                	add    al,BYTE PTR [eax]

Disassembly of section .gnu.version_r:

08048340 <.gnu.version_r>:
 8048340:	01 00                	add    DWORD PTR [eax],eax
 8048342:	01 00                	add    DWORD PTR [eax],eax
 8048344:	10 00                	adc    BYTE PTR [eax],al
 8048346:	00 00                	add    BYTE PTR [eax],al
 8048348:	10 00                	adc    BYTE PTR [eax],al
 804834a:	00 00                	add    BYTE PTR [eax],al
 804834c:	00 00                	add    BYTE PTR [eax],al
 804834e:	00 00                	add    BYTE PTR [eax],al
 8048350:	10 69 69             	adc    BYTE PTR [ecx+0x69],ch
 8048353:	0d 00 00 02 00       	or     eax,0x20000
 8048358:	7f 00                	jg     804835a <mmap@plt-0xae>
 804835a:	00 00                	add    BYTE PTR [eax],al
 804835c:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .rel.dyn:

08048360 <.rel.dyn>:
 8048360:	98                   	cwde   
 8048361:	9a 04 08 06 03 00 00 	call   0x0:0x3060804
 8048368:	e0 9a                	loopne 8048304 <mmap@plt-0x104>
 804836a:	04 08                	add    al,0x8
 804836c:	05 0d 00 00 00       	add    eax,0xd
 8048371:	9b                   	fwait
 8048372:	04 08                	add    al,0x8
 8048374:	05                   	.byte 0x5
 8048375:	0b 00                	or     eax,DWORD PTR [eax]
	...

Disassembly of section .rel.plt:

08048378 <.rel.plt>:
 8048378:	a8 9a                	test   al,0x9a
 804837a:	04 08                	add    al,0x8
 804837c:	07                   	pop    es
 804837d:	01 00                	add    DWORD PTR [eax],eax
 804837f:	00 ac 9a 04 08 07 02 	add    BYTE PTR [edx+ebx*4+0x2070804],ch
 8048386:	00 00                	add    BYTE PTR [eax],al
 8048388:	b0 9a                	mov    al,0x9a
 804838a:	04 08                	add    al,0x8
 804838c:	07                   	pop    es
 804838d:	03 00                	add    eax,DWORD PTR [eax]
 804838f:	00 b4 9a 04 08 07 04 	add    BYTE PTR [edx+ebx*4+0x4070804],dh
 8048396:	00 00                	add    BYTE PTR [eax],al
 8048398:	b8 9a 04 08 07       	mov    eax,0x708049a
 804839d:	05 00 00 bc 9a       	add    eax,0x9abc0000
 80483a2:	04 08                	add    al,0x8
 80483a4:	07                   	pop    es
 80483a5:	06                   	push   es
 80483a6:	00 00                	add    BYTE PTR [eax],al
 80483a8:	c0 9a 04 08 07 07 00 	rcr    BYTE PTR [edx+0x7070804],0x0
 80483af:	00 c4                	add    ah,al
 80483b1:	9a 04 08 07 08 00 00 	call   0x0:0x8070804
 80483b8:	c8 9a 04 08          	enter  0x49a,0x8
 80483bc:	07                   	pop    es
 80483bd:	09 00                	or     DWORD PTR [eax],eax
 80483bf:	00 cc                	add    ah,cl
 80483c1:	9a 04 08 07 0a 00 00 	call   0x0:0xa070804

Disassembly of section .init:

080483c8 <.init>:
 80483c8:	55                   	push   ebp
 80483c9:	89 e5                	mov    ebp,esp
 80483cb:	53                   	push   ebx
 80483cc:	83 ec 04             	sub    esp,0x4
 80483cf:	e8 00 00 00 00       	call   80483d4 <mmap@plt-0x34>
 80483d4:	5b                   	pop    ebx
 80483d5:	81 c3 c8 16 00 00    	add    ebx,0x16c8
 80483db:	8b 93 fc ff ff ff    	mov    edx,DWORD PTR [ebx-0x4]
 80483e1:	85 d2                	test   edx,edx
 80483e3:	74 05                	je     80483ea <mmap@plt-0x1e>
 80483e5:	e8 3e 00 00 00       	call   8048428 <__gmon_start__@plt>
 80483ea:	e8 51 01 00 00       	call   8048540 <sleep@plt+0xa8>
 80483ef:	e8 bc 04 00 00       	call   80488b0 <sleep@plt+0x418>
 80483f4:	58                   	pop    eax
 80483f5:	5b                   	pop    ebx
 80483f6:	c9                   	leave  
 80483f7:	c3                   	ret    

Disassembly of section .plt:

080483f8 <mmap@plt-0x10>:
 80483f8:	ff 35 a0 9a 04 08    	push   DWORD PTR ds:0x8049aa0
 80483fe:	ff 25 a4 9a 04 08    	jmp    DWORD PTR ds:0x8049aa4
 8048404:	00 00                	add    BYTE PTR [eax],al
	...

08048408 <mmap@plt>:
 8048408:	ff 25 a8 9a 04 08    	jmp    DWORD PTR ds:0x8049aa8
 804840e:	68 00 00 00 00       	push   0x0
 8048413:	e9 e0 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048418 <getpid@plt>:
 8048418:	ff 25 ac 9a 04 08    	jmp    DWORD PTR ds:0x8049aac
 804841e:	68 08 00 00 00       	push   0x8
 8048423:	e9 d0 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048428 <__gmon_start__@plt>:
 8048428:	ff 25 b0 9a 04 08    	jmp    DWORD PTR ds:0x8049ab0
 804842e:	68 10 00 00 00       	push   0x10
 8048433:	e9 c0 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048438 <calloc@plt>:
 8048438:	ff 25 b4 9a 04 08    	jmp    DWORD PTR ds:0x8049ab4
 804843e:	68 18 00 00 00       	push   0x18
 8048443:	e9 b0 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048448 <memset@plt>:
 8048448:	ff 25 b8 9a 04 08    	jmp    DWORD PTR ds:0x8049ab8
 804844e:	68 20 00 00 00       	push   0x20
 8048453:	e9 a0 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048458 <__libc_start_main@plt>:
 8048458:	ff 25 bc 9a 04 08    	jmp    DWORD PTR ds:0x8049abc
 804845e:	68 28 00 00 00       	push   0x28
 8048463:	e9 90 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048468 <memcpy@plt>:
 8048468:	ff 25 c0 9a 04 08    	jmp    DWORD PTR ds:0x8049ac0
 804846e:	68 30 00 00 00       	push   0x30
 8048473:	e9 80 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048478 <fwrite@plt>:
 8048478:	ff 25 c4 9a 04 08    	jmp    DWORD PTR ds:0x8049ac4
 804847e:	68 38 00 00 00       	push   0x38
 8048483:	e9 70 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048488 <fprintf@plt>:
 8048488:	ff 25 c8 9a 04 08    	jmp    DWORD PTR ds:0x8049ac8
 804848e:	68 40 00 00 00       	push   0x40
 8048493:	e9 60 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

08048498 <sleep@plt>:
 8048498:	ff 25 cc 9a 04 08    	jmp    DWORD PTR ds:0x8049acc
 804849e:	68 48 00 00 00       	push   0x48
 80484a3:	e9 50 ff ff ff       	jmp    80483f8 <mmap@plt-0x10>

Disassembly of section .text:

## CHANGED HEADER
080484b0 <_start>:
 80484b0:	31 ed                	xor    ebp,ebp
 80484b2:	5e                   	pop    esi
 80484b3:	89 e1                	mov    ecx,esp
 80484b5:	83 e4 f0             	and    esp,0xfffffff0
 80484b8:	50                   	push   eax
 80484b9:	54                   	push   esp
 80484ba:	52                   	push   edx
 80484bb:	68 40 88 04 08       	push   0x8048840
 80484c0:	68 50 88 04 08       	push   0x8048850
 80484c5:	51                   	push   ecx
 80484c6:	56                   	push   esi
 80484c7:	68 cd 87 04 08       	push   0x80487cd
 80484cc:	e8 87 ff ff ff       	call   8048458 <__libc_start_main@plt>
 80484d1:	f4                   	hlt    
 80484d2:	90                   	nop
 80484d3:	90                   	nop
 80484d4:	90                   	nop
 80484d5:	90                   	nop
 80484d6:	90                   	nop
 80484d7:	90                   	nop
 80484d8:	90                   	nop
 80484d9:	90                   	nop
 80484da:	90                   	nop
 80484db:	90                   	nop
 80484dc:	90                   	nop
 80484dd:	90                   	nop
 80484de:	90                   	nop
 80484df:	90                   	nop
 
## ADDED HEADER
08048390 <__do_global_dtors_aux>:
 80484e0:	55                   	push   ebp
 80484e1:	89 e5                	mov    ebp,esp
 80484e3:	53                   	push   ebx
 80484e4:	83 ec 04             	sub    esp,0x4
 80484e7:	80 3d 04 9b 04 08 00 	cmp    BYTE PTR ds:0x8049b04,0x0
 80484ee:	75 40                	jne    8048530 <sleep@plt+0x98>
 80484f0:	8b 15 08 9b 04 08    	mov    edx,DWORD PTR ds:0x8049b08
 80484f6:	b8 c8 99 04 08       	mov    eax,0x80499c8
 80484fb:	2d c4 99 04 08       	sub    eax,0x80499c4
 8048500:	c1 f8 02             	sar    eax,0x2
 8048503:	8d 58 ff             	lea    ebx,[eax-0x1]
 8048506:	39 da                	cmp    edx,ebx
 8048508:	73 1f                	jae    8048529 <sleep@plt+0x91>
 804850a:	8d b6 00 00 00 00    	lea    esi,[esi+0x0]
 8048510:	8d 42 01             	lea    eax,[edx+0x1]
 8048513:	a3 08 9b 04 08       	mov    ds:0x8049b08,eax
 8048518:	ff 14 85 c4 99 04 08 	call   DWORD PTR [eax*4+0x80499c4]
 804851f:	8b 15 08 9b 04 08    	mov    edx,DWORD PTR ds:0x8049b08
 8048525:	39 da                	cmp    edx,ebx
 8048527:	72 e7                	jb     8048510 <sleep@plt+0x78>
 8048529:	c6 05 04 9b 04 08 01 	mov    BYTE PTR ds:0x8049b04,0x1
 8048530:	83 c4 04             	add    esp,0x4
 8048533:	5b                   	pop    ebx
 8048534:	5d                   	pop    ebp
 8048535:	c3                   	ret    
 8048536:	8d 76 00             	lea    esi,[esi+0x0]
 8048539:	8d bc 27 00 00 00 00 	lea    edi,[edi+eiz*1+0x0]
 
 ## ADDED HEADER
08048540 <frame_dummy>:
 8048540:	55                   	push   ebp
 8048541:	89 e5                	mov    ebp,esp
 8048543:	83 ec 08             	sub    esp,0x8
 8048546:	a1 cc 99 04 08       	mov    eax,ds:0x80499cc
 804854b:	85 c0                	test   eax,eax
 804854d:	74 12                	je     8048561 <sleep@plt+0xc9>
 804854f:	b8 00 00 00 00       	mov    eax,0x0
 8048554:	85 c0                	test   eax,eax
 8048556:	74 09                	je     8048561 <sleep@plt+0xc9>
 8048558:	c7 04 24 cc 99 04 08 	mov    DWORD PTR [esp],0x80499cc
 804855f:	ff d0                	call   eax
 8048561:	c9                   	leave  
 8048562:	c3                   	ret    
 8048563:	90                   	nop
 
 ## FUNCTION 1 (Creates a bunch of structs in a loop)
 8048564:	55                   	push   ebp
 8048565:	89 e5                	mov    ebp,esp
 8048567:	83 ec 38             	sub    esp,0x38
 ## Local vars (0x4-0x18)?
 804856a:	c7 45 f4 00 00 00 00 	mov    DWORD PTR [ebp-0xc],0x0  ## counter?
 8048571:	c7 45 f8 00 00 00 00 	mov    DWORD PTR [ebp-0x8],0x0  ## mmap_addr
 8048578:	c7 45 fc 00 00 00 00 	mov    DWORD PTR [ebp-0x4],0x0  ## next_addr
 ## local struct (or variables resembling)
 804857f:	c7 45 e4 31 23 00 00 	mov    DWORD PTR [ebp-0x1c],0x2331  # struct.x = 9009
 8048586:	c7 45 ec 00 00 00 00 	mov    DWORD PTR [ebp-0x14],0x0     # struct.hang = 0;
 804858d:	c7 45 f0 00 00 00 00 	mov    DWORD PTR [ebp-0x10],0x0     # struct.next = 0;
 8048594:	c7 45 e8 00 00 00 00 	mov    DWORD PTR [ebp-0x18],0x0     # struct.prev = 0;
 
 ## Call to MMAP, store in 0x8 
 ## mmap(NULL, 2048, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) 
 804859b:	c7 44 24 14 00 00 00 	mov    DWORD PTR [esp+0x14],0x0			# offset = 0
 80485a2:	00 
 80485a3:	c7 44 24 10 ff ff ff 	mov    DWORD PTR [esp+0x10],0xffffffff	#file descriptor = -1
 80485aa:	ff 
 80485ab:	c7 44 24 0c 22 00 00 	mov    DWORD PTR [esp+0xc],0x22			#param4: flags
 80485b2:	00 															#MAP_PRIVATE|MAP_ANONYMOUS
 80485b3:	c7 44 24 08 03 00 00 	mov    DWORD PTR [esp+0x8],0x3			#param3: int prot 
 80485ba:	00 																#PROT_READ|PROT_WRITE
 80485bb:	c7 44 24 04 00 08 00 	mov    DWORD PTR [esp+0x4],0x800		#param2: size = 2048
 80485c2:	00 
 80485c3:	c7 04 24 00 00 00 00 	mov    DWORD PTR [esp],0x0        		#param1: addr* = NULL
 80485ca:	e8 39 fe ff ff       	call   8048408 <mmap@plt>
 80485cf:	89 45 f8             	mov    DWORD PTR [ebp-0x8],eax  ## save mmap address in 0x8
 
  ## FPRINTF Function call
  ## printf(stdout, mystery string, mmap_addr)
 80485d2:	8b 15 00 9b 04 08    	mov    edx,DWORD PTR ds:0x8049b00 ## standard out
 80485d8:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]    ## eax = mmap addr
 80485db:	89 44 24 08          	mov    DWORD PTR [esp+0x8],eax    ## param3: mmap addr
 80485df:	c7 44 24 04 0d 89 04 	mov    DWORD PTR [esp+0x4],0x804890d ## param2: string
 80485e6:	08 
 80485e7:	89 14 24             	mov    DWORD PTR [esp],edx		 ## param1: std out
 80485ea:	e8 99 fe ff ff       	call   8048488 <fprintf@plt>
 
 ## Memset function call
 ## memset(mmap_addr, 0, 2048)
 80485ef:	c7 44 24 08 00 08 00 	mov    DWORD PTR [esp+0x8],0x800
 80485f6:	00 
 80485f7:	c7 44 24 04 00 00 00 	mov    DWORD PTR [esp+0x4],0x0
 80485fe:	00 
 80485ff:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]
 8048602:	89 04 24             	mov    DWORD PTR [esp],eax
 8048605:	e8 3e fe ff ff       	call   8048448 <memset@plt>
 
 ## initalize loop
 804860a:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8] # eax = mmap_addr
 804860d:	89 45 fc             	mov    DWORD PTR [ebp-0x4],eax # next_addr = mmap_addr
 8048610:	c7 45 f4 00 00 00 00 	mov    DWORD PTR [ebp-0xc],0x0 # counter = 0
 
 ## LOOP 
 8048617:	eb 40                	jmp    8048659 <sleep@plt+0x1c1> ## goto loop gaurd
 ## Loop Body
 8048619:	8b 45 e4             	mov    eax,DWORD PTR [ebp-0x1c]  ## eax = struct.x
 804861c:	03 45 f4             	add    eax,DWORD PTR [ebp-0xc]   ## eax = struct.x+ counter
 804861f:	89 45 e4             	mov    DWORD PTR [ebp-0x1c],eax  ## struct.x += counter
 8048622:	8b 45 fc             	mov    eax,DWORD PTR [ebp-0x4]   ## eax = next_addr
 8048625:	89 45 e8             	mov    DWORD PTR [ebp-0x18],eax  ## struct.prev = next_addr
 8048628:	8b 45 fc             	mov    eax,DWORD PTR [ebp-0x4]	 ## eax = next_addr
 804862b:	89 45 ec             	mov    DWORD PTR [ebp-0x14],eax  ## struct.hang = next_addr
 804862e:	8b 45 fc             	mov    eax,DWORD PTR [ebp-0x4]   ## eax = next_addr
 8048631:	83 c0 14             	add    eax,0x14                  ## eax = next_addr + 20
 8048634:	89 45 f0             	mov    DWORD PTR [ebp-0x10],eax  ## struct.next = next_addr+20
 
 ## Call mem copy
 8048637:	c7 44 24 08 10 00 00 	mov    DWORD PTR [esp+0x8],0x10 ## size_t = 16
 804863e:	00 
 804863f:	8d 45 e4             	lea    eax,[ebp-0x1c]           ## eax = &struct
 8048642:	89 44 24 04          	mov    DWORD PTR [esp+0x4],eax  ## src* = &struct
 8048646:	8b 45 fc             	mov    eax,DWORD PTR [ebp-0x4]  ## eax = next_addr
 8048649:	89 04 24             	mov    DWORD PTR [esp],eax      ## dest = next_addr
 804864c:	e8 17 fe ff ff       	call   8048468 <memcpy@plt>
 ## Increment counter
 8048651:	83 45 fc 14          	add    DWORD PTR [ebp-0x4],0x14 ## next_addr += 20
 8048655:	83 45 f4 01          	add    DWORD PTR [ebp-0xc],0x1  # counter += 1
 ## loop gaurd
 8048659:	83 7d f4 18          	cmp    DWORD PTR [ebp-0xc],0x18		## if counter <= 24
 804865d:	7e ba                	jle    8048619 <sleep@plt+0x181>  	## goto top
 ## end loop															## else leave
 804865f:	c9                   	leave  
 8048660:	c3                   	ret 
 ## end function 1
 
 
 
 #####################
 ## FUNCTION 2   
 8048661:	55                   	push   ebp
 8048662:	89 e5                	mov    ebp,esp
 8048664:	83 ec 28             	sub    esp,0x28
 ## possible struct? 
 8048667:	c7 45 f0 00 00 00 00 	mov    DWORD PTR [ebp-0x10],0x0 ## stuct *ptr1
 804866e:	c7 45 f4 00 00 00 00 	mov    DWORD PTR [ebp-0xc],0x0  ## struct *tmp_ptr
 8048675:	c7 45 f8 00 00 00 00 	mov    DWORD PTR [ebp-0x8],0x0  ## struct *ptr2
 804867c:	c7 45 fc 00 00 00 00 	mov    DWORD PTR [ebp-0x4],0x0  ## counter
 ## A global, possibly static 
 8048683:	c7 05 0c 9b 04 08 00 	mov    DWORD PTR ds:0x8049b0c,0x0 ## global struct *g_ptr
 804868a:	00 00 00 
 ##initalize loop
 804868d:	c7 45 fc 00 00 00 00 	mov    DWORD PTR [ebp-0x4],0x0  ## counter = 0
 ## goto loop gaurd
 8048694:	e9 df 00 00 00       	jmp    8048778 <sleep@plt+0x2e0>
  ## LOOP TOP
  
  ## Calloc Call 1 | ptr1 = calloc(1, sizeof(struct));
 8048699:	c7 44 24 04 10 00 00 	mov    DWORD PTR [esp+0x4],0x10 ## size = 16
 80486a0:	00 
 80486a1:	c7 04 24 01 00 00 00 	mov    DWORD PTR [esp],0x1 ## nmenb = 1
 80486a8:	e8 8b fd ff ff       	call   8048438 <calloc@plt>
 80486ad:	89 45 f0             	mov    DWORD PTR [ebp-0x10],eax ## ptr1 = calloc(...);
 
 ## Calloc call 2 | ptr2 = calloc(1, sizeof(struct));
 80486b0:	c7 44 24 04 10 00 00 	mov    DWORD PTR [esp+0x4],0x10 ## size = 16
 80486b7:	00 
 80486b8:	c7 04 24 01 00 00 00 	mov    DWORD PTR [esp],0x1  ## nmemb = 1
 80486bf:	e8 74 fd ff ff       	call   8048438 <calloc@plt>
 80486c4:	89 45 f8             	mov    DWORD PTR [ebp-0x8],eax ## ptr2 = calloc(...);
 
 ## Null error checking
 ## if ptr1 == NULL || ptr2 == null
 80486c7:	83 7d f0 00          	cmp    DWORD PTR [ebp-0x10],0x0
 80486cb:	74 06                	je     80486d3 <sleep@plt+0x23b>
 80486cd:	83 7d f8 00          	cmp    DWORD PTR [ebp-0x8],0x0 
 80486d1:	75 2a                	jne    80486fd <sleep@plt+0x265>
 ## if body
 ## Pretty sure this is a GCC optimization to write a string
 80486d3:	a1 e0 9a 04 08       	mov    eax,ds:0x8049ae0 ## eax = stderr
 80486d8:	89 44 24 0c          	mov    DWORD PTR [esp+0xc],eax ## FILE* = strerr
 80486dc:	c7 44 24 08 23 00 00 	mov    DWORD PTR [esp+0x8],0x23  ## nmemb = 35
 80486e3:	00 
 80486e4:	c7 44 24 04 01 00 00 	mov    DWORD PTR [esp+0x4],0x1  ## size = 1 
 80486eb:	00 
 80486ec:	c7 04 24 18 89 04 08 	mov    DWORD PTR [esp],0x8048918 ## ptr = some string
 80486f3:	e8 80 fd ff ff       	call   8048478 <fwrite@plt>		 ## ptr string was oped out
 80486f8:	e9 86 00 00 00       	jmp    8048783 <sleep@plt+0x2eb> ## break/goto end
 ## end if
 
 80486fd:	8b 45 fc             	mov    eax,DWORD PTR [ebp-0x4]  ## eax = counter
 8048700:	8d 50 64             	lea    edx,[eax+0x64]			## edx = counter + 100 
 8048703:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]  ## eax  ptr2
 8048706:	89 10                	mov    DWORD PTR [eax],edx      ## ptr2->x = counter + 100
 8048708:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]  ## eax = ptr2
 804870b:	c7 40 08 00 00 00 00 	mov    DWORD PTR [eax+0x8],0x0  ## ptr2->hang = 0
 8048712:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]  ## eax = ptr2
 8048715:	c7 40 0c 00 00 00 00 	mov    DWORD PTR [eax+0xc],0x0  ## ptr2->next = 0
 804871c:	8b 55 f8             	mov    edx,DWORD PTR [ebp-0x8]  ## edx = ptr2
 804871f:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10] ## eax = ptr1
 8048722:	89 42 04             	mov    DWORD PTR [edx+0x4],eax  ## ptr2->prev = ptr1
 8048725:	8b 55 f0             	mov    edx,DWORD PTR [ebp-0x10] ## edx = ptr1
 8048728:	8b 45 fc             	mov    eax,DWORD PTR [ebp-0x4]  ## eax = counter
 804872b:	89 02                	mov    DWORD PTR [edx],eax      ## ptr1->x = counter
 804872d:	8b 55 f0             	mov    edx,DWORD PTR [ebp-0x10] ## edx = ptr1
 8048730:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]  ## eax = ptr2
 8048733:	89 42 08             	mov    DWORD PTR [edx+0x8],eax  ## ptr1->hang = ptr2
 8048736:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10] ## eax = ptr1
 8048739:	c7 40 0c 00 00 00 00 	mov    DWORD PTR [eax+0xc],0x0  ## ptr1->next = 0
 ## if counter == 0
 ## if the first iteration
 8048740:	83 7d fc 00          	cmp    DWORD PTR [ebp-0x4],0x0   # if counter == 0
 8048744:	75 16                	jne    804875c <sleep@plt+0x2c4> ## goto else
 ## if body
 8048746:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]  ## eax = ptr1
 8048749:	a3 0c 9b 04 08       	mov    ds:0x8049b0c,eax          ## g_ptr = ptr1
 804874e:	a1 0c 9b 04 08       	mov    eax,ds:0x8049b0c          ## eax = g_ptr
 8048753:	c7 40 04 00 00 00 00 	mov    DWORD PTR [eax+0x4],0x0   ## g_ptr->prev = 0
 804875a:	eb 12                	jmp    804876e <sleep@plt+0x2d6> ## jump past else
 ## endif
 ## else counter > 0
 804875c:	8b 55 f0             	mov    edx,DWORD PTR [ebp-0x10]   ## edx = ptr1
 804875f:	8b 45 f4             	mov    eax,DWORD PTR [ebp-0xc]    ## eax = tmp_ptr
 8048762:	89 42 04             	mov    DWORD PTR [edx+0x4],eax    ## ptr1->prev = tmp_ptr
 8048765:	8b 55 f4             	mov    edx,DWORD PTR [ebp-0xc]    ## edx = tmp_ptr
 8048768:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]   ## eax = ptr1
 804876b:	89 42 0c             	mov    DWORD PTR [edx+0xc],eax    ## tmp_ptr->next = ptr1 
 ## end else
 804876e:	8b 45 f0             	mov    eax,DWORD PTR [ebp-0x10]   ## eax = ptr1
 8048771:	89 45 f4             	mov    DWORD PTR [ebp-0xc],eax    ## tmp_ptr = ptr1
 ## LOOP INC COUNTER
 8048774:	83 45 fc 01          	add    DWORD PTR [ebp-0x4],0x1 ##counter += 1;
 ## LOOP GAURD
 8048778:	83 7d fc 18          	cmp    DWORD PTR [ebp-0x4],0x18 ## if ( counter <= 24 )
 804877c:	0f 8e 17 ff ff ff    	jle    8048699 <sleep@plt+0x201> ## then goto loop top
 8048782:	90                   	nop
 ## end loop
 8048783:	c9                   	leave  
 8048784:	c3                   	ret
 
 ## FUNCTION 3    
 8048785:	55                   	push   ebp
 8048786:	89 e5                	mov    ebp,esp
 8048788:	83 ec 08             	sub    esp,0x8 ## GCC uses unneeded stack space!
 804878b:	e8 d1 fe ff ff       	call   8048661 <sleep@plt+0x1c9> # call function 2
 8048790:	e8 cf fd ff ff       	call   8048564 <sleep@plt+0xcc> # Call function 1
 8048795:	c9                   	leave  
 8048796:	c3                   	ret   
 ## END FUNCTION 3
 
 
 ## FUNCTION 4 
 8048797:	55                   	push   ebp
 8048798:	89 e5                	mov    ebp,esp
 804879a:	83 ec 18             	sub    esp,0x18
 804879d:	c7 45 f8 dd be ce 50 	mov    DWORD PTR [ebp-0x8],0x50cebedd  # x = 1355726557
 80487a4:	c7 45 fc 01 00 00 00 	mov    DWORD PTR [ebp-0x4],0x1         # count = 1
 80487ab:	c7 45 f8 ef be ad de 	mov    DWORD PTR [ebp-0x8],0xdeadbeef  # x = 0xdeadbeef 
 80487b2:	e8 ce ff ff ff       	call   8048785 <sleep@plt+0x2ed>  # call function 3
 80487b7:	eb 0c                	jmp    80487c5 <sleep@plt+0x32d>  # goto loop gaurd
 ## loop body
 ## sleep(1);
 80487b9:	c7 04 24 01 00 00 00 	mov    DWORD PTR [esp],0x1      
 80487c0:	e8 d3 fc ff ff       	call   8048498 <sleep@plt>
 ## loop gaurd
 80487c5:	83 7d fc 01          	cmp    DWORD PTR [ebp-0x4],0x1  ## if count = 1
 80487c9:	74 ee                	je     80487b9 <sleep@plt+0x321> ## goto loop top
 80487cb:	c9                   	leave  ## else leave
 80487cc:	c3                   	ret
 ## END FUNCTION 4
 
 
 ## MAIN
080487cd <main>:
 80487cd:	8d 4c 24 04          	lea    ecx,[esp+0x4] # Align the stack
 80487d1:	83 e4 f0             	and    esp,0xfffffff0
 80487d4:	ff 71 fc             	push   DWORD PTR [ecx-0x4]
 80487d7:	55                   	push   ebp
 80487d8:	89 e5                	mov    ebp,esp
 80487da:	51                   	push   ecx
 80487db:	83 ec 24             	sub    esp,0x24 ## make room for 36 bytes
 ## START USER CODE
 
 ## START SECTION WTF???
 80487de:	c7 45 f8 01 c0 00 00 	mov    DWORD PTR [ebp-0x8],0xc001  # var_x = 49153
 80487e5:	8b 45 f8             	mov    eax,DWORD PTR [ebp-0x8]     # eax = var_x
 80487e8:	a3 14 9b 04 08       	mov    ds:0x8049b14,eax			   # static s1 = eax
 80487ed:	a1 14 9b 04 08       	mov    eax,ds:0x8049b14            # eax = &c
 80487f2:	c1 e0 10             	shl    eax,0x10					   # c << 0x10
 80487f5:	a3 14 9b 04 08       	mov    ds:0x8049b14,eax			   # c = c << 0x10
 80487fa:	8b 15 14 9b 04 08    	mov    edx,DWORD PTR ds:0x8049b14  # edx = *c
 8048800:	a1 d8 9a 04 08       	mov    eax, ds:0x8049ad8
 8048805:	09 d0                	or     eax,edx
 8048807:	a3 14 9b 04 08       	mov    ds:0x8049b14,eax
 ## END WTF???
 
 804880c:	e8 07 fc ff ff       	call   8048418 <getpid@plt>
 8048811:	8b 15 00 9b 04 08    	mov    edx,DWORD PTR ds:0x8049b00     ## standard out
 8048817:	89 44 24 08          	mov    DWORD PTR [esp+0x8],eax        ## param3: tmp_int = pid
 804881b:	c7 44 24 04 3c 89 04 	mov    DWORD PTR [esp+0x4],0x804893c  ## param2: char* ptr
 8048822:	08 
 8048823:	89 14 24             	mov    DWORD PTR [esp],edx            ## param1: stdout
 8048826:	e8 5d fc ff ff       	call   8048488 <fprintf@plt>
 
 ## setup memory
 804882b:	e8 67 ff ff ff       	call   8048797 <sleep@plt+0x2ff>  #  call function 4
 
 ## end main
 ## return 1
 8048830:	b8 00 00 00 00       	mov    eax,0x0
 8048835:	83 c4 24             	add    esp,0x24
 8048838:	59                   	pop    ecx
 8048839:	5d                   	pop    ebp
 804883a:	8d 61 fc             	lea    esp,[ecx-0x4]
 804883d:	c3                   	ret    
 804883e:	90                   	nop
 804883f:	90                   	nop
 
 ## Can't ID this function
 8048840:	55                   	push   ebp
 8048841:	89 e5                	mov    ebp,esp
 8048843:	5d                   	pop    ebp
 8048844:	c3                   	ret    
 8048845:	8d 74 26 00          	lea    esi,[esi+eiz*1+0x0]
 8048849:	8d bc 27 00 00 00 00 	lea    edi,[edi+eiz*1+0x0]
 
## ADDED HEADER
08048850 <__libc_csu_init>:
 8048850:	55                   	push   ebp
 8048851:	89 e5                	mov    ebp,esp
 8048853:	57                   	push   edi
 8048854:	56                   	push   esi
 8048855:	53                   	push   ebx
 8048856:	e8 4f 00 00 00       	call   80488aa <sleep@plt+0x412>
 804885b:	81 c3 41 12 00 00    	add    ebx,0x1241
 8048861:	83 ec 0c             	sub    esp,0xc
 8048864:	e8 5f fb ff ff       	call   80483c8 <mmap@plt-0x40>
 8048869:	8d bb 20 ff ff ff    	lea    edi,[ebx-0xe0]
 804886f:	8d 83 20 ff ff ff    	lea    eax,[ebx-0xe0]
 8048875:	29 c7                	sub    edi,eax
 8048877:	c1 ff 02             	sar    edi,0x2
 804887a:	85 ff                	test   edi,edi
 804887c:	74 24                	je     80488a2 <sleep@plt+0x40a>
 804887e:	31 f6                	xor    esi,esi
 8048880:	8b 45 10             	mov    eax,DWORD PTR [ebp+0x10]
 8048883:	89 44 24 08          	mov    DWORD PTR [esp+0x8],eax
 8048887:	8b 45 0c             	mov    eax,DWORD PTR [ebp+0xc]
 804888a:	89 44 24 04          	mov    DWORD PTR [esp+0x4],eax
 804888e:	8b 45 08             	mov    eax,DWORD PTR [ebp+0x8]
 8048891:	89 04 24             	mov    DWORD PTR [esp],eax
 8048894:	ff 94 b3 20 ff ff ff 	call   DWORD PTR [ebx+esi*4-0xe0]
 804889b:	83 c6 01             	add    esi,0x1
 804889e:	39 fe                	cmp    esi,edi
 80488a0:	72 de                	jb     8048880 <sleep@plt+0x3e8>
 80488a2:	83 c4 0c             	add    esp,0xc
 80488a5:	5b                   	pop    ebx
 80488a6:	5e                   	pop    esi
 80488a7:	5f                   	pop    edi
 80488a8:	5d                   	pop    ebp
 80488a9:	c3                   	ret
 
 ## ADDED HEADER
080488b0 <__i686.get_pc_thunk.bx>  
 80488aa:	8b 1c 24             	mov    ebx,DWORD PTR [esp]
 80488ad:	c3                   	ret    
 80488ae:	90                   	nop
 80488af:	90                   	nop
 
 ## ADDED HEADER
080488b0 <__do_global_ctors_aux>:
 80488b0:	55                   	push   ebp
 80488b1:	89 e5                	mov    ebp,esp
 80488b3:	53                   	push   ebx
 80488b4:	83 ec 04             	sub    esp,0x4
 80488b7:	a1 bc 99 04 08       	mov    eax,ds:0x80499bc
 80488bc:	83 f8 ff             	cmp    eax,0xffffffff
 80488bf:	74 13                	je     80488d4 <sleep@plt+0x43c>
 80488c1:	bb bc 99 04 08       	mov    ebx,0x80499bc
 80488c6:	66 90                	xchg   ax,ax
 80488c8:	83 eb 04             	sub    ebx,0x4
 80488cb:	ff d0                	call   eax
 80488cd:	8b 03                	mov    eax,DWORD PTR [ebx]
 80488cf:	83 f8 ff             	cmp    eax,0xffffffff
 80488d2:	75 f4                	jne    80488c8 <sleep@plt+0x430>
 80488d4:	83 c4 04             	add    esp,0x4
 80488d7:	5b                   	pop    ebx
 80488d8:	5d                   	pop    ebp
 80488d9:	c3                   	ret    
 80488da:	90                   	nop
 80488db:	90                   	nop

Disassembly of section .fini:

080488dc <.fini>:
 80488dc:	55                   	push   ebp
 80488dd:	89 e5                	mov    ebp,esp
 80488df:	53                   	push   ebx
 80488e0:	83 ec 04             	sub    esp,0x4
 80488e3:	e8 00 00 00 00       	call   80488e8 <sleep@plt+0x450>
 80488e8:	5b                   	pop    ebx
 80488e9:	81 c3 b4 11 00 00    	add    ebx,0x11b4
 80488ef:	e8 ec fb ff ff       	call   80484e0 <sleep@plt+0x48>
 80488f4:	59                   	pop    ecx
 80488f5:	5b                   	pop    ebx
 80488f6:	c9                   	leave  
 80488f7:	c3                   	ret    

Disassembly of section .rodata:

080488f8 <_IO_stdin_used-0x4>:
 80488f8:	03 00                	add    eax,DWORD PTR [eax]
	...

080488fc <_IO_stdin_used>:
 80488fc:	01 00                	add    DWORD PTR [eax],eax
 80488fe:	02 00                	add    al,BYTE PTR [eax]
 8048900:	00 00                	add    BYTE PTR [eax],al
 8048902:	00 00                	add    BYTE PTR [eax],al
 ## password starts here 
 8048904:	70 61                	jo     8048967 <_IO_stdin_used+0x6b>
 8048906:	73 73                	jae    804897b <_IO_stdin_used+0x7f>
 8048908:	77 6f                	ja     8048979 <_IO_stdin_used+0x7d>
 804890a:	72 64                	jb     8048970 <_IO_stdin_used+0x74>
 804890c:	00 63 20             	add    BYTE PTR [ebx+0x20],ah
 804890f:	3d 20 25 70 0a       	cmp    eax,0xa702520
 8048914:	00 00                	add    BYTE PTR [eax],al
 8048916:	00 00                	add    BYTE PTR [eax],al
 8048918:	66 61                	popaw  
 804891a:	69 6c 65 64 20 74 6f 	imul   ebp,DWORD PTR [ebp+eiz*2+0x64],0x206f7420
 8048921:	20 
 8048922:	63 61 6c             	arpl   WORD PTR [ecx+0x6c],sp
 8048925:	6c                   	ins    BYTE PTR es:[edi],dx
 8048926:	6f                   	outs   dx,DWORD PTR ds:[esi]
 8048927:	63 2f                	arpl   WORD PTR [edi],bp
 8048929:	63 72 65             	arpl   WORD PTR [edx+0x65],si
 804892c:	61                   	popa   
 804892d:	74 65                	je     8048994 <_IO_stdin_used+0x98>
 804892f:	20 61 20             	and    BYTE PTR [ecx+0x20],ah
 8048932:	53                   	push   ebx
 8048933:	6f                   	outs   dx,DWORD PTR ds:[esi]
 8048934:	6d                   	ins    DWORD PTR es:[edi],dx
 8048935:	65                   	gs
 8048936:	4e                   	dec    esi
 8048937:	6f                   	outs   dx,DWORD PTR ds:[esi]
 8048938:	64 65 0a 00          	fs or  al,BYTE PTR fs:gs:[eax]
 804893c:	70 69                	jo     80489a7 <_IO_stdin_used+0xab>
 804893e:	64 20 3d 20 25 64 0a 	and    BYTE PTR fs:0xa642520,bh
	...

Disassembly of section .eh_frame_hdr:

08048948 <.eh_frame_hdr>:
 8048948:	01 1b                	add    DWORD PTR [ebx],ebx
 804894a:	03 3b                	add    edi,DWORD PTR [ebx]
 804894c:	18 00                	sbb    BYTE PTR [eax],al
 804894e:	00 00                	add    BYTE PTR [eax],al
 8048950:	02 00                	add    al,BYTE PTR [eax]
 8048952:	00 00                	add    BYTE PTR [eax],al
 8048954:	f8                   	clc    
 8048955:	fe                   	(bad)  
 8048956:	ff                   	(bad)  
 8048957:	ff 34 00             	push   DWORD PTR [eax+eax*1]
 804895a:	00 00                	add    BYTE PTR [eax],al
 804895c:	08 ff                	or     bh,bh
 804895e:	ff                   	(bad)  
 804895f:	ff 50 00             	call   DWORD PTR [eax+0x0]
	...

Disassembly of section .eh_frame:

08048964 <.eh_frame>:
 8048964:	14 00                	adc    al,0x0
 8048966:	00 00                	add    BYTE PTR [eax],al
 8048968:	00 00                	add    BYTE PTR [eax],al
 804896a:	00 00                	add    BYTE PTR [eax],al
 804896c:	01 7a 52             	add    DWORD PTR [edx+0x52],edi
 804896f:	00 01                	add    BYTE PTR [ecx],al
 8048971:	7c 08                	jl     804897b <_IO_stdin_used+0x7f>
 8048973:	01 1b                	add    DWORD PTR [ebx],ebx
 8048975:	0c 04                	or     al,0x4
 8048977:	04 88                	add    al,0x88
 8048979:	01 00                	add    DWORD PTR [eax],eax
 804897b:	00 18                	add    BYTE PTR [eax],bl
 804897d:	00 00                	add    BYTE PTR [eax],al
 804897f:	00 1c 00             	add    BYTE PTR [eax+eax*1],bl
 8048982:	00 00                	add    BYTE PTR [eax],al
 8048984:	bc fe ff ff 05       	mov    esp,0x5fffffe
 8048989:	00 00                	add    BYTE PTR [eax],al
 804898b:	00 00                	add    BYTE PTR [eax],al
 804898d:	41                   	inc    ecx
 804898e:	0e                   	push   cs
 804898f:	08 85 02 42 0d 05    	or     BYTE PTR [ebp+0x50d4202],al
 8048995:	00 00                	add    BYTE PTR [eax],al
 8048997:	00 1c 00             	add    BYTE PTR [eax+eax*1],bl
 804899a:	00 00                	add    BYTE PTR [eax],al
 804899c:	38 00                	cmp    BYTE PTR [eax],al
 804899e:	00 00                	add    BYTE PTR [eax],al
 80489a0:	b0 fe                	mov    al,0xfe
 80489a2:	ff                   	(bad)  
 80489a3:	ff 5a 00             	call   FWORD PTR [edx+0x0]
 80489a6:	00 00                	add    BYTE PTR [eax],al
 80489a8:	00 41 0e             	add    BYTE PTR [ecx+0xe],al
 80489ab:	08 85 02 42 0d 05    	or     BYTE PTR [ebp+0x50d4202],al
 80489b1:	43                   	inc    ebx
 80489b2:	83 05 86 04 87 03 00 	add    DWORD PTR ds:0x3870486,0x0
 80489b9:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .ctors:

080499bc <.ctors>:
 80499bc:	ff                   	(bad)  
 80499bd:	ff                   	(bad)  
 80499be:	ff                   	(bad)  
 80499bf:	ff 00                	inc    DWORD PTR [eax]
 80499c1:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .dtors:

080499c4 <.dtors>:
 80499c4:	ff                   	(bad)  
 80499c5:	ff                   	(bad)  
 80499c6:	ff                   	(bad)  
 80499c7:	ff 00                	inc    DWORD PTR [eax]
 80499c9:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .jcr:

080499cc <.jcr>:
 80499cc:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .dynamic:

080499d0 <.dynamic>:
 80499d0:	01 00                	add    DWORD PTR [eax],eax
 80499d2:	00 00                	add    BYTE PTR [eax],al
 80499d4:	10 00                	adc    BYTE PTR [eax],al
 80499d6:	00 00                	add    BYTE PTR [eax],al
 80499d8:	0c 00                	or     al,0x0
 80499da:	00 00                	add    BYTE PTR [eax],al
 80499dc:	c8 83 04 08          	enter  0x483,0x8
 80499e0:	0d 00 00 00 dc       	or     eax,0xdc000000
 80499e5:	88 04 08             	mov    BYTE PTR [eax+ecx*1],al
 80499e8:	f5                   	cmc    
 80499e9:	fe                   	(bad)  
 80499ea:	ff 6f 8c             	jmp    FWORD PTR [edi-0x74]
 80499ed:	81 04 08 05 00 00 00 	add    DWORD PTR [eax+ecx*1],0x5
 80499f4:	98                   	cwde   
 80499f5:	82                   	(bad)  
 80499f6:	04 08                	add    al,0x8
 80499f8:	06                   	push   es
 80499f9:	00 00                	add    BYTE PTR [eax],al
 80499fb:	00 b8 81 04 08 0a    	add    BYTE PTR [eax+0xa080481],bh
 8049a01:	00 00                	add    BYTE PTR [eax],al
 8049a03:	00 89 00 00 00 0b    	add    BYTE PTR [ecx+0xb000000],cl
 8049a09:	00 00                	add    BYTE PTR [eax],al
 8049a0b:	00 10                	add    BYTE PTR [eax],dl
 8049a0d:	00 00                	add    BYTE PTR [eax],al
 8049a0f:	00 15 00 00 00 00    	add    BYTE PTR ds:0x0,dl
 8049a15:	00 00                	add    BYTE PTR [eax],al
 8049a17:	00 03                	add    BYTE PTR [ebx],al
 8049a19:	00 00                	add    BYTE PTR [eax],al
 8049a1b:	00 9c 9a 04 08 02 00 	add    BYTE PTR [edx+ebx*4+0x20804],bl
 8049a22:	00 00                	add    BYTE PTR [eax],al
 8049a24:	50                   	push   eax
 8049a25:	00 00                	add    BYTE PTR [eax],al
 8049a27:	00 14 00             	add    BYTE PTR [eax+eax*1],dl
 8049a2a:	00 00                	add    BYTE PTR [eax],al
 8049a2c:	11 00                	adc    DWORD PTR [eax],eax
 8049a2e:	00 00                	add    BYTE PTR [eax],al
 8049a30:	17                   	pop    ss
 8049a31:	00 00                	add    BYTE PTR [eax],al
 8049a33:	00 78 83             	add    BYTE PTR [eax-0x7d],bh
 8049a36:	04 08                	add    al,0x8
 8049a38:	11 00                	adc    DWORD PTR [eax],eax
 8049a3a:	00 00                	add    BYTE PTR [eax],al
 8049a3c:	60                   	pusha  
 8049a3d:	83 04 08 12          	add    DWORD PTR [eax+ecx*1],0x12
 8049a41:	00 00                	add    BYTE PTR [eax],al
 8049a43:	00 18                	add    BYTE PTR [eax],bl
 8049a45:	00 00                	add    BYTE PTR [eax],al
 8049a47:	00 13                	add    BYTE PTR [ebx],dl
 8049a49:	00 00                	add    BYTE PTR [eax],al
 8049a4b:	00 08                	add    BYTE PTR [eax],cl
 8049a4d:	00 00                	add    BYTE PTR [eax],al
 8049a4f:	00 fe                	add    dh,bh
 8049a51:	ff                   	(bad)  
 8049a52:	ff 6f 40             	jmp    FWORD PTR [edi+0x40]
 8049a55:	83 04 08 ff          	add    DWORD PTR [eax+ecx*1],0xffffffff
 8049a59:	ff                   	(bad)  
 8049a5a:	ff 6f 01             	jmp    FWORD PTR [edi+0x1]
 8049a5d:	00 00                	add    BYTE PTR [eax],al
 8049a5f:	00 f0                	add    al,dh
 8049a61:	ff                   	(bad)  
 8049a62:	ff 6f 22             	jmp    FWORD PTR [edi+0x22]
 8049a65:	83 04 08 00          	add    DWORD PTR [eax+ecx*1],0x0
	...

Disassembly of section .got:

08049a98 <.got>:
 8049a98:	00 00                	add    BYTE PTR [eax],al
	...

Disassembly of section .got.plt:

08049a9c <.got.plt>:
 8049a9c:	d0 99 04 08 00 00    	rcr    BYTE PTR [ecx+0x804],1
 8049aa2:	00 00                	add    BYTE PTR [eax],al
 8049aa4:	00 00                	add    BYTE PTR [eax],al
 8049aa6:	00 00                	add    BYTE PTR [eax],al
 8049aa8:	0e                   	push   cs
 8049aa9:	84 04 08             	test   BYTE PTR [eax+ecx*1],al
 8049aac:	1e                   	push   ds
 8049aad:	84 04 08             	test   BYTE PTR [eax+ecx*1],al
 8049ab0:	2e 84 04 08          	test   BYTE PTR cs:[eax+ecx*1],al
 8049ab4:	3e 84 04 08          	test   BYTE PTR ds:[eax+ecx*1],al
 8049ab8:	4e                   	dec    esi
 8049ab9:	84 04 08             	test   BYTE PTR [eax+ecx*1],al
 8049abc:	5e                   	pop    esi
 8049abd:	84 04 08             	test   BYTE PTR [eax+ecx*1],al
 8049ac0:	6e                   	outs   dx,BYTE PTR ds:[esi]
 8049ac1:	84 04 08             	test   BYTE PTR [eax+ecx*1],al
 8049ac4:	7e 84                	jle    8049a4a <_IO_stdin_used+0x114e>
 8049ac6:	04 08                	add    al,0x8
 8049ac8:	8e 84 04 08 9e 84 04 	mov    es,WORD PTR [esp+eax*1+0x4849e08]
 8049acf:	08                   	.byte 0x8

Disassembly of section .data:

08049ad0 <.data>:
 8049ad0:	00 00                	add    BYTE PTR [eax],al
 8049ad2:	00 00                	add    BYTE PTR [eax],al
 8049ad4:	04 89                	add    al,0x89
 8049ad6:	04 08                	add    al,0x8
 8049ad8:	fe ca                	dec    dl
	...

Disassembly of section .bss:

08049ae0 <stderr>:
	...

08049b00 <stdout>:
	...

Disassembly of section .comment:

00000000 <.comment>:
   0:	00 47 43             	add    BYTE PTR [edi+0x43],al
   3:	43                   	inc    ebx
   4:	3a 20                	cmp    ah,BYTE PTR [eax]
   6:	28 47 4e             	sub    BYTE PTR [edi+0x4e],al
   9:	55                   	push   ebp
   a:	29 20                	sub    DWORD PTR [eax],esp
   c:	34 2e                	xor    al,0x2e
   e:	33 2e                	xor    ebp,DWORD PTR [esi]
  10:	32 20                	xor    ah,BYTE PTR [eax]
  12:	32 30                	xor    dh,BYTE PTR [eax]
  14:	30 38                	xor    BYTE PTR [eax],bh
  16:	31 31                	xor    DWORD PTR [ecx],esi
  18:	30 35 20 28 52 65    	xor    BYTE PTR ds:0x65522820,dh
  1e:	64 20 48 61          	and    BYTE PTR fs:[eax+0x61],cl
  22:	74 20                	je     44 <mmap@plt-0x80483c4>
  24:	34 2e                	xor    al,0x2e
  26:	33 2e                	xor    ebp,DWORD PTR [esi]
  28:	32 2d 37 29 00 00    	xor    ch,BYTE PTR ds:0x2937
  2e:	47                   	inc    edi
  2f:	43                   	inc    ebx
  30:	43                   	inc    ebx
  31:	3a 20                	cmp    ah,BYTE PTR [eax]
  33:	28 47 4e             	sub    BYTE PTR [edi+0x4e],al
  36:	55                   	push   ebp
  37:	29 20                	sub    DWORD PTR [eax],esp
  39:	34 2e                	xor    al,0x2e
  3b:	33 2e                	xor    ebp,DWORD PTR [esi]
  3d:	32 20                	xor    ah,BYTE PTR [eax]
  3f:	32 30                	xor    dh,BYTE PTR [eax]
  41:	30 38                	xor    BYTE PTR [eax],bh
  43:	31 31                	xor    DWORD PTR [ecx],esi
  45:	30 35 20 28 52 65    	xor    BYTE PTR ds:0x65522820,dh
  4b:	64 20 48 61          	and    BYTE PTR fs:[eax+0x61],cl
  4f:	74 20                	je     71 <mmap@plt-0x8048397>
  51:	34 2e                	xor    al,0x2e
  53:	33 2e                	xor    ebp,DWORD PTR [esi]
  55:	32 2d 37 29 00 00    	xor    ch,BYTE PTR ds:0x2937
  5b:	47                   	inc    edi
  5c:	43                   	inc    ebx
  5d:	43                   	inc    ebx
  5e:	3a 20                	cmp    ah,BYTE PTR [eax]
  60:	28 47 4e             	sub    BYTE PTR [edi+0x4e],al
  63:	55                   	push   ebp
  64:	29 20                	sub    DWORD PTR [eax],esp
  66:	34 2e                	xor    al,0x2e
  68:	33 2e                	xor    ebp,DWORD PTR [esi]
  6a:	32 20                	xor    ah,BYTE PTR [eax]
  6c:	32 30                	xor    dh,BYTE PTR [eax]
  6e:	30 38                	xor    BYTE PTR [eax],bh
  70:	31 31                	xor    DWORD PTR [ecx],esi
  72:	30 35 20 28 52 65    	xor    BYTE PTR ds:0x65522820,dh
  78:	64 20 48 61          	and    BYTE PTR fs:[eax+0x61],cl
  7c:	74 20                	je     9e <mmap@plt-0x804836a>
  7e:	34 2e                	xor    al,0x2e
  80:	33 2e                	xor    ebp,DWORD PTR [esi]
  82:	32 2d 37 29 00 00    	xor    ch,BYTE PTR ds:0x2937
  88:	47                   	inc    edi
  89:	43                   	inc    ebx
  8a:	43                   	inc    ebx
  8b:	3a 20                	cmp    ah,BYTE PTR [eax]
  8d:	28 47 4e             	sub    BYTE PTR [edi+0x4e],al
  90:	55                   	push   ebp
  91:	29 20                	sub    DWORD PTR [eax],esp
  93:	34 2e                	xor    al,0x2e
  95:	33 2e                	xor    ebp,DWORD PTR [esi]
  97:	32 20                	xor    ah,BYTE PTR [eax]
  99:	32 30                	xor    dh,BYTE PTR [eax]
  9b:	30 38                	xor    BYTE PTR [eax],bh
  9d:	31 31                	xor    DWORD PTR [ecx],esi
  9f:	30 35 20 28 52 65    	xor    BYTE PTR ds:0x65522820,dh
  a5:	64 20 48 61          	and    BYTE PTR fs:[eax+0x61],cl
  a9:	74 20                	je     cb <mmap@plt-0x804833d>
  ab:	34 2e                	xor    al,0x2e
  ad:	33 2e                	xor    ebp,DWORD PTR [esi]
  af:	32 2d 37 29 00 00    	xor    ch,BYTE PTR ds:0x2937
  b5:	47                   	inc    edi
  b6:	43                   	inc    ebx
  b7:	43                   	inc    ebx
  b8:	3a 20                	cmp    ah,BYTE PTR [eax]
  ba:	28 47 4e             	sub    BYTE PTR [edi+0x4e],al
  bd:	55                   	push   ebp
  be:	29 20                	sub    DWORD PTR [eax],esp
  c0:	34 2e                	xor    al,0x2e
  c2:	33 2e                	xor    ebp,DWORD PTR [esi]
  c4:	32 20                	xor    ah,BYTE PTR [eax]
  c6:	32 30                	xor    dh,BYTE PTR [eax]
  c8:	30 38                	xor    BYTE PTR [eax],bh
  ca:	31 31                	xor    DWORD PTR [ecx],esi
  cc:	30 35 20 28 52 65    	xor    BYTE PTR ds:0x65522820,dh
  d2:	64 20 48 61          	and    BYTE PTR fs:[eax+0x61],cl
  d6:	74 20                	je     f8 <mmap@plt-0x8048310>
  d8:	34 2e                	xor    al,0x2e
  da:	33 2e                	xor    ebp,DWORD PTR [esi]
  dc:	32 2d 37 29 00 00    	xor    ch,BYTE PTR ds:0x2937
  e2:	47                   	inc    edi
  e3:	43                   	inc    ebx
  e4:	43                   	inc    ebx
  e5:	3a 20                	cmp    ah,BYTE PTR [eax]
  e7:	28 47 4e             	sub    BYTE PTR [edi+0x4e],al
  ea:	55                   	push   ebp
  eb:	29 20                	sub    DWORD PTR [eax],esp
  ed:	34 2e                	xor    al,0x2e
  ef:	33 2e                	xor    ebp,DWORD PTR [esi]
  f1:	32 20                	xor    ah,BYTE PTR [eax]
  f3:	32 30                	xor    dh,BYTE PTR [eax]
  f5:	30 38                	xor    BYTE PTR [eax],bh
  f7:	31 31                	xor    DWORD PTR [ecx],esi
  f9:	30 35 20 28 52 65    	xor    BYTE PTR ds:0x65522820,dh
  ff:	64 20 48 61          	and    BYTE PTR fs:[eax+0x61],cl
 103:	74 20                	je     125 <mmap@plt-0x80482e3>
 105:	34 2e                	xor    al,0x2e
 107:	33 2e                	xor    ebp,DWORD PTR [esi]
 109:	32                   	.byte 0x32
 10a:	2d                   	.byte 0x2d
 10b:	37                   	aaa    
 10c:	29 00                	sub    DWORD PTR [eax],eax
