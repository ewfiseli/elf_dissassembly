#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

typedef struct _somenode {
int x;
struct _somenode* prev;
struct _somenode* hang;
struct _somenode* next;
} SomeNode;

// possibly just SomeNode g_node; (not pointer);
SomeNode *g_ptr;

/*
	I think this creates the poorly formed group
	see note on other function.
*/
#define AREA_SIZE 2048
void create_bad_group(void)
{
	int counter;
	void *mmap_addr, *next_addr;
	SomeNode node;
	node.x = 0x2331; // 9009
	node.hang = 0;
	node.next = 0;
	node.prev = 0;
	
	// this translation is rock solid
	mmap_addr = mmap(NULL, AREA_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
	fprintf(stdout, "c = 0x%lx\n", (unsigned long) mmap_addr);
	memset(mmap_addr, 0, AREA_SIZE);
	
	// any errors in this section are based on initialization order
	next_addr = mmap_addr;
	counter = 0;
	while (counter < 25) {
		node.x = node.x + counter;
		node.prev = next_addr;
		node.hang = next_addr;
		node.next = next_addr+20;
		
		memcpy(next_addr, (void*) &node, sizeof(SomeNode));
		
		next_addr += 20;
		counter += 1;
	}
	
}

/*  I think this is the function that creates the "good" group
	note to reader: this evaluation was made with 4 hours sleep
*/
void create_good_group(void)
{
	SomeNode *ptr1, *ptr2, *tmp_ptr;
	int counter;
	
	ptr1 = 0;
	ptr2 = 0;
	tmp_ptr = 0;
	g_ptr = 0;
	counter = 0;
	
	while (counter < 25) {
		// good translation
		ptr1 = calloc(1, sizeof(SomeNode));
		ptr2 = calloc(1, sizeof(SomeNode));
		
		// good translation, boolean short circuting in interesting
		if (ptr1 == NULL || ptr2 == NULL) {
			fprintf(stderr, "Some Error string about null pointers here\n");
			break;
		}
		
		ptr2->x = counter + 100;
		ptr2->hang = 0;
		ptr2->next = 0;
		ptr2->prev = ptr1;
		
		ptr1->x = counter;
		ptr1->hang = ptr2;
		ptr1->next = 0;
		
		if (counter == 0) {
			// g_ptr should be able to act as a nice
			// anchor to find this group
			g_ptr = ptr1;
			g_ptr->prev = 0;
		} else {
			ptr1->prev = tmp_ptr;
			tmp_ptr->next = ptr1;
		}
		
		tmp_ptr = ptr1;
		++counter;
	}
}

void create_groups(void)
{
	// order may be reversed in assembly
	// but I don't think it matters
	create_bad_group();
	create_good_group();
}


void create_and_loop()
{
	// possibly long? I need to look at 64 bit for this. 
	long x;
	// loop represents a bool type
	int loop;
	
	x = 0x50cebedd; // not sure what this is
	loop = 1;       // always loop
	x = 0xdeadbeef; // found deadbeef! 
	
	create_groups(); // create node groups
	
	// loop forever!
	while (loop) {
		sleep(1);
	}
}

int main()
{
	/* 	some string manipulation or things I don't understand
	 	skipping it, maybe "password" is here; but I doubt it.
	 	need to look into the way GCC handles string literals.
	*/
	fprintf(stdout, "pid = %u\n", getpid());
	
	create_and_loop();
	
	return 0;
}
